LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

MARKUP_LIBS := libsketchology_native.so
MARKUP_SYMLINKS := $(addprefix $(TARGET_OUT_PRODUCT_APPS)/MarkupGoogle/lib/arm64/,$(notdir $(MARKUP_LIBS)))
$(MARKUP_SYMLINKS): $(LOCAL_INSTALLED_MODULE)
	@echo "MARKUP lib link: $@"
	@mkdir -p $(dir $@)
	@rm -rf $@
	$(hide) ln -sf /product/lib64/$(notdir $@) $@

ALL_DEFAULT_INSTALLED_MODULES += $(MARKUP_SYMLINKS)
