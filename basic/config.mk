PRODUCT_SOONG_NAMESPACES += \
    vendor/gapps/basic

PRODUCT_COPY_FILES += \
    vendor/gapps/basic/product/etc/sysconfig/nexus.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nexus.xml \
    vendor/gapps/basic/product/etc/sysconfig/pixel_2018_exclusive.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_2018_exclusive.xml \
    vendor/gapps/basic/product/etc/sysconfig/pixel_experience_2017.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2017.xml \
    vendor/gapps/basic/product/etc/sysconfig/pixel_experience_2018.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2018.xml \
    vendor/gapps/basic/product/etc/sysconfig/wellbeing.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/wellbeing.xml \
    vendor/gapps/basic/product/lib64/libjni_latinimegoogle.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libjni_latinimegoogle.so \
    vendor/gapps/basic/product/lib64/libsketchology_native.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libsketchology_native.so \
    vendor/gapps/basic/product/lib/libjni_latinimegoogle.so:$(TARGET_COPY_OUT_PRODUCT)/lib/libjni_latinimegoogle.so

PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    GoogleBackupTransport \
    GoogleCalendarSyncAdapter \
    GoogleContacts \
    GoogleDialer \
    GoogleRestore \
    MarkupGoogle \
    PartnerSetupPrebuilt \
    Photos \
    PrebuiltBugle \
    SetupWizard \
    SoundPickerGooglePrebuilt \
    Velvet \
    WellbeingPrebuilt

# Gms overlay
PRODUCT_PACKAGES += \
    GmsConfigOverlayComms

# Properties
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true \
    ro.setupwizard.rotation_locked=true \
    setupwizard.theme=glif_v3_light

ifeq ($(PRODUCT_GMS_CLIENTID_BASE),)
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=android-google
else
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=$(PRODUCT_GMS_CLIENTID_BASE)
endif

$(call inherit-product, vendor/gapps/core/config.mk)
